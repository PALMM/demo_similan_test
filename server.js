//npm init -y สร้าง package.json
//npm i express mysql nodemon cors

const express = require('express')
const cors = require('cors');
const mysql = require('mysql')

const app = express()
app.use(cors());
app.use(express.json())

//MySQL connection
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'user_info_test'
})

connection.connect((err) => {
    if(err){
        console.log('Error connecting to MySQL database = ', err)
        return
    }
    console.log('MySQL successfully connected')
})

//CREATE Routes
app.post("/create", async (req, res) => {
    const {id, name, address, postcode, phone, fax, email} = req.body

    try{
        connection.query(
            "INSERT INTO user(cust_id, cust_name, cust_address, cust_postcode, cust_phone, cust_fax, cust_email) VALUES(?, ?, ?, ?, ?, ?, ?)",
            [id, name, address, postcode, phone, fax, email],
            (err, results, fields) => {
                if(err){
                    console.log("Error while inserting a user into the database ", err)
                    return res.status(400).send()
                }
                return res.status(201).json({ message: "New user successfully created!"})
            }
        )
    }catch(err){
        console.log(err)
        return res.status(500).send()
    }
})

//READ
app.get("/read" ,async (req, res) => {
    try{
        connection.query("SELECT * FROM user", (err, result, fields) => {
            if(err){
                console.log(err)
                return res.status(400).send()
            }
            res.status(200).json(result)
        })
    }catch(err){
        console.log(err)
        return res.status(500).send()
    }
})

//READ Single user from db
app.get("/read/single/:email" ,async (req, res) => {

    const email = req.params.email

    try{
        connection.query("SELECT * FROM user WHERE cust_email = ?", [ email ], (err, result, fields) => {
            if(err){
                console.log(err)
                return res.status(400).send()
            }
            res.status(200).json(result)
        })
    }catch(err){
        console.log(err)
        return res.status(500).send()
    }
})

// EDIT Route
app.put("/update/:id", async (req, res) => {
    const { id } = req.params;
    const { name, address, postcode, phone, fax, email } = req.body;
    console.log(name)
    try {
        connection.query(
            "UPDATE user SET cust_name = ?, cust_address = ?, cust_postcode = ?, cust_phone = ?, cust_fax = ?, cust_email = ? WHERE cust_id = ?",
            [name, address, postcode, phone, fax, email, id],
            (err, result) => {
                if (err) {
                    console.log("Error while updating user: ", err);
                    return res.status(400).send();
                }
                return res.status(200).json({ message: "User updated successfully!" });
            }
        );
    } catch (err) {
        console.log(err);
        return res.status(500).send();
    }
});

// DELETE Route
app.delete("/delete/:id", async (req, res) => {
    const { id } = req.params;

    try {
        connection.query("DELETE FROM user WHERE cust_id = ?", [id], (err, result) => {
            if (err) {
                console.log("Error while deleting user: ", err);
                return res.status(400).send();
            }
            return res.status(200).json({ message: "User deleted successfully!" });
        });
    } catch (err) {
        console.log(err);
        return res.status(500).send();
    }
});
      
app.listen(8000, () => console.log('Server is running on port 8000'))